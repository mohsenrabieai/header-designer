var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cons = require('consolidate');
var fs = require('fs');

var toolboxData = fs.readFileSync('data/toolbox.json', 'utf8');
var gridData = fs.readFileSync('data/grid.json', 'utf8');

var app = express();
var portNo = 8080;

app.engine('html', cons.lodash);
app.set('view engine', 'html');
app.set('views', __dirname);

app.use(express.static('lib'));
app.use(express.static('dist'));
app.use(express.static('style'));

app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.render('index', {
    cache: false,
    data: JSON.stringify({
      toolbox: JSON.parse(toolboxData),
      grid: JSON.parse(gridData)
    })
  });
});

app.listen(portNo, function () {
	console.log('Listening on port ' + portNo + '...');
});
