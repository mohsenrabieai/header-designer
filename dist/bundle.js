(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var PubSub = require('./pubsub');
var Toolbox = require('./toolbox');
var Container = require('./container');

function onMouseDown(e) {
  PubSub.publish('window-mousedown', e);
}

function onMouseUp(e) {
  PubSub.publish('window-mouseup', e);
}

function onMouseMove(e) {
  PubSub.publish('window-mousemove', e);
}

module.exports = {
  controller: function(args) {
    var toolbox = args.toolbox,
        grid = args.grid;

    return {
      toolbox: m.prop(toolbox),
      grid: m.prop(grid),

      registerGlobalEvents: function(elem, isInit, ctx, vdom) {
        if (!isInit) {
          window.addEventListener('mousedown', onMouseDown, false);
          window.addEventListener('mouseup', onMouseUp, false);
          window.addEventListener('mousemove', onMouseMove, false);
        }

        ctx.onunload = function() {
          window.removeEventListener('mousedown', onMouseDown, false);
          window.removeEventListener('mouseup', onMouseUp, false);
          window.removeEventListener('mousemove', onMouseMove, false);
        }
      }
    };
  },

  view: function(ctrl, args) {
    return m('.app', {
      config: ctrl.registerGlobalEvents
    }, [
      // m(Toolbox, ctrl.toolbox()),
      m(Container, ctrl.grid())
    ]);
  }
};

},{"./container":4,"./pubsub":9,"./toolbox":11}],2:[function(require,module,exports){
var ConfigurableBox = require('./configurable-box');
var Element = require('./element');

module.exports = {
  minWidth: m.prop(100 / 12),

  validate: function(column) {
    return column || {};
  },

  controller: function(args) {
    var column = args.column,
        ctrl;

    ctrl = {
      setComponent: function(component) {
        component = Element.validate(component);
        column.element = component;

        m.redraw();
      }
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    var column = args.column,
        element = args.column.element || {},
        columnOptions = column && column.options ? column.options : {},
        padding = columnOptions.padding || 0;

    return m('.column-wrapper', {
      key: column.id,
      style: {
        width: args.width + '%'
      }
    }, [
      m(ConfigurableBox, {
        attrs: {
          className: 'column'
        },
        contents: element.type ? m(Element, { column: args.column }) : null
      })
    ]);
  }
};

},{"./configurable-box":3,"./element":6}],3:[function(require,module,exports){
var PubSub = require('./pubsub');

var lastHighlightedCtrl;

var ConfigurableBox = module.exports = {
  controller: function(args) {
    var ctrl = {
      isHighlighted: m.prop(false),

      highlight: function(e) {
        e.stopPropagation();

        if (lastHighlightedCtrl) {
          lastHighlightedCtrl.isHighlighted(false);
        }

        ctrl.isHighlighted(true);
        lastHighlightedCtrl = ctrl;

        m.redraw();
      },

      onOptionsClick: function(e) {
        if (typeof args.attrs.onoptionsclick === 'function') {
          args.attrs.onoptionsclick(e);
        }
      }
    };

    PubSub.subscribe('window-mousedown', function(e) {
      ctrl.isHighlighted(false);
      m.redraw();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    var className = '.configurable-box' + (ctrl.isHighlighted() ? ' highlighted' : '');

    return m(className, Object.assign({
      onmousedown: ctrl.highlight
    }, args.attrs), [
      args.contents,
      ctrl.isHighlighted() ? m('.options-tab', [
        m('a.options-link', {
          href: '#',
          onclick: ctrl.onOptionsClick
        }, 'options')
      ]) : null
    ]);
  }
};

},{"./pubsub":9}],4:[function(require,module,exports){
var ConfigurableBox = require('./configurable-box');
var Row = require('./row');

var Container = module.exports = {
  controller: function(args) {
    var container = args,
        ctrl;

    ctrl = {
      container: m.prop(container),
      currentRowId: m.prop(0),

      getNewRowId: function() {
        return ctrl.currentRowId(ctrl.currentRowId() + 1);
      },

      addRow: function(row) {
        row = row || {};
        row.id = ctrl.getNewRowId();
        ctrl.container().rows.push(row);
        m.redraw();
      },

      removeRow: function(id) {
        ctrl.container().rows = ctrl.container().rows.filter(function(row) {
          return row.id != id;
        });

        m.redraw();
      },

      showOptionsMenu: function(e) {
        console.log('container clicked.');
      }
    };

    ctrl.container().rows.forEach(function(row) {
      row.id = ctrl.getNewRowId();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    return m(ConfigurableBox, {
      attrs: {
        className: 'container',
        onoptionsclick: ctrl.showOptionsMenu
      },
      contents: ctrl.container().rows.map(function(row) {
        return m(Row, { row: row });
      })
    });
  }
};

},{"./configurable-box":3,"./row":10}],5:[function(require,module,exports){
module.exports = {
  view: function(ctrl, args) {
    var width = 4;

    return m('.dragbar', {
      style: {
        top: 0,
        bottom: 0,
        left: 'calc(' + args.pos + '% - ' + (width / 2) + 'px)',
        width: width + 'px'
      },
      onmousedown: function(e) {
        args.onDragStart(e, args.row, args.colIndex);
      }
    });
  }
};

},{}],6:[function(require,module,exports){
var ConfigurableBox = require('./configurable-box');

var Element = module.exports = {
  validate: function(element) {
    return element || {};
  },

  controller: function(args) {
    var ctrl = {
      column: m.prop(args.column),
      element: m.prop(Element.validate(args.column.element))
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    var className = 'element';

    return m(ConfigurableBox, {
      attrs: {
        className: className
      },
      contents: ctrl.element().type ? ctrl.column() : null
    });
  }
};

},{"./configurable-box":3}],7:[function(require,module,exports){
var VM = function(pos) {
  var vm = {
    x: m.prop(pos.x),
    y: m.prop(pos.y),
    zIndex: m.prop(VM.lastMaxZIndex),
    dragStartInfo: m.prop(null)
  };

  return vm;
};

VM.lastMaxZIndex = 10000;

module.exports = {
  controller: function(args) {
    var vm = VM(args.pos);

    var ctrl = {
      x: vm.x,
      y: vm.y,
      zIndex: vm.zIndex,
      dragStartInfo: vm.dragStartInfo,

      onDragStart: function(e) {
        function onDragging(e) {
          ctrl.x(ctrl.dragStartInfo().x + e.clientX - ctrl.dragStartInfo().clientX);
          ctrl.y(ctrl.dragStartInfo().y + e.clientY - ctrl.dragStartInfo().clientY);

          m.redraw();
        }

        function onDragEnd(e) {
          document.removeEventListener('mousemove', onDragging, true);
          document.removeEventListener('mouseup', onDragEnd, true);
        }

        e.preventDefault();

        ctrl.dragStartInfo({
          x: ctrl.x(),
          y: ctrl.y(),
          clientX: e.clientX,
          clientY: e.clientY
        });

        document.addEventListener('mousemove', onDragging, true);
        document.addEventListener('mouseup', onDragEnd, true);

        ctrl.lastDocumentCursor = document.documentElement.style.cursor;
        document.documentElement.style.cursor = 'move';

        ctrl.zIndex(VM.lastMaxZIndex++);

        m.redraw();
      }
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    return m('.float-window', {
      style: {
        left: ctrl.x() + 'px',
        top: ctrl.y() + 'px',
        zIndex: ctrl.zIndex()
      }
    }, [
      m('.float-window-titlebar', {
        onmousedown: ctrl.onDragStart
      }, args.title),
      m('.float-window-content', args.content)
    ]);
  }
};

},{}],8:[function(require,module,exports){
var App = require('./app');

var Builder = {
  controller: function() {
    var data = bsData;

    return {
      data: m.prop(data)
    };
  },

  view: function(ctrl) {
    return m(App, ctrl.data());
  }
};

m.mount(document.body, Builder);

},{"./app":1}],9:[function(require,module,exports){
var topics = {};
var hOP = topics.hasOwnProperty;

module.exports = {
  subscribe: function(topic, listener) {
    // Create the topic's object if not yet created
    if(!hOP.call(topics, topic)) topics[topic] = [];

    // Add the listener to queue
    var index = topics[topic].push(listener) -1;

    // Provide handle back for removal of topic
    return {
      remove: function() {
        delete topics[topic][index];
      }
    };
  },

  publish: function(topic, info) {
    // If the topic doesn't exist, or there's no listeners in queue, just leave
    if(!hOP.call(topics, topic)) return;

    // Cycle through topics queue, fire!
    topics[topic].forEach(function(item) {
        item(info != undefined ? info : {});
    });
  }
};

},{}],10:[function(require,module,exports){
var PubSub = require('./pubsub');
var ConfigurableBox = require('./configurable-box');
var Column = require('./column');
var Dragbar = require('./dragbar');

var Row = module.exports = {
  validate: function(row) {
    var options = row.options || {},
        columnsNo = options.columnsNo || 1,
        columns = row.columns || [];

    for (var i = 0; i < columnsNo; i++) {
      columns[i] = Column.validate(columns[i]);
    }

    row.columns = columns;
  },

  controller: function(args) {
    var row = args.row,
        ctrl;

    ctrl = {
      lastDragStartInfo: m.prop(null),
      currentColId: m.prop(0),

      getNewColId: function() {
        return ctrl.currentColId(ctrl.currentColId() + 1);
      },

      setColumns: function(columns) {
        if (!columns || columns.length < 1) {
          columns = [{}];
        }

        row.columns = columns;
        row.options.columnsNo = columns.length;
        m.redraw();
      },

      validateColumnsWidth: function() {
        var options = row.options || {},
            columnsNo = options.columnsNo || 1,
            sumOfColumnsWithWidth = 0,
            columnsWithWidthNo = 0;

        for (var i = 0; i < columnsNo; i++) {
          var column = row.columns[i] || {},
              width = parseFloat(column.width);

          if (width) {
            sumOfColumnsWithWidth += width;
            columnsWithWidthNo++;
          }

          row.columns[i] = column;
        }

        var widthOfColumnsWithNoWidth = (100 - sumOfColumnsWithWidth) / (columnsNo - columnsWithWidthNo);

        for (var i = 0, length = row.columns.length; i < length; i++) {
          row.columns[i].width = row.columns[i].width || widthOfColumnsWithNoWidth;
        }
      },

      onDragStart: function(e, row, colIndex) {
        e.preventDefault();
        e.stopPropagation();

        function onDragging(e) {
          var dragStartInfo = ctrl.lastDragStartInfo(),
              colIndex = dragStartInfo.colIndex,
              rowWidth = dragStartInfo.rowWidth,
              moveX = e.clientX - dragStartInfo.clientX,
              colMinWidth = Column.minWidth(),
              sumOfTwoCols = dragStartInfo.currentColWidth + dragStartInfo.nextColWidth,
              percent = moveX / rowWidth * 100;

          if (dragStartInfo.currentColWidth + percent < colMinWidth) {
            row.columns[colIndex].width = colMinWidth;
            row.columns[colIndex + 1].width = sumOfTwoCols - colMinWidth;
          } else if (dragStartInfo.nextColWidth - percent < colMinWidth) {
            row.columns[colIndex].width = sumOfTwoCols - colMinWidth;
            row.columns[colIndex + 1].width = colMinWidth;
          } else {
            row.columns[colIndex].width = dragStartInfo.currentColWidth + percent;
            row.columns[colIndex + 1].width = dragStartInfo.nextColWidth - percent;
          }

          m.redraw();
        }

        function onDragEnd(e) {
          mouseMoveHandler.remove();
          mouseUpHandler.remove();

          ctrl.lastDragStartInfo(null);

          document.documentElement.style.cursor = ctrl.lastDocumentCursor;
        }

        ctrl.lastDragStartInfo({
          rowWidth: e.target.parentNode.offsetWidth,
          currentColWidth: row.columns[colIndex].width,
          nextColWidth: row.columns[colIndex + 1].width,
          colIndex: colIndex,
          offsetX: e.offsetX,
          offsetY: e.offsetY,
          clientX: e.clientX,
          clientY: e.clientY
        });

        var mouseMoveHandler = PubSub.subscribe('window-mousemove', onDragging);
        var mouseUpHandler = PubSub.subscribe('window-mouseup', onDragEnd);

        ctrl.lastDocumentCursor = document.documentElement.style.cursor;
        document.documentElement.style.cursor = 'col-resize';
      }
    };

    Row.validate(row);

    row.columns.forEach(function(column) {
      column.id = ctrl.getNewColId();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    var row = args.row,
        elems = [],
        options = row.options || {},
        columnsNo = options.columnsNo || 1,
        widthSum = 0;

    ctrl.validateColumnsWidth();

    for (var colIndex = 0; colIndex < columnsNo; colIndex++) {
      var column = row.columns[colIndex],
          columnWidth = column.width;

      row.columns[colIndex].width = columnWidth;
      widthSum += columnWidth;

      elems.push(m(Column, {
        column: column,
        width: columnWidth
      }));

      if (colIndex < columnsNo - 1) {
        elems.push(m(Dragbar, {
          row: row,
          pos: widthSum,
          colIndex: colIndex,
          onDragStart: ctrl.onDragStart
        }));
      }
    }

    return m(ConfigurableBox, {
      attrs: {
        className: 'row-wrapper'
      },
      contents: m('.row', elems)
    });
  }
};

},{"./column":2,"./configurable-box":3,"./dragbar":5,"./pubsub":9}],11:[function(require,module,exports){
var FloatWindow = require('./float-window');
var Element = require('./element');

module.exports = {
  view: function(ctrl, args) {
    return m(FloatWindow, {
      title: 'Elements List',
      pos: {
        x: 170,
        y: 150
      },
      content: args.tools.map(function(tool) {
        return m(Element, tool);
      })
    });
  }
};

},{"./element":6,"./float-window":7}]},{},[8])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYXBwLmpzIiwic3JjL2NvbHVtbi5qcyIsInNyYy9jb25maWd1cmFibGUtYm94LmpzIiwic3JjL2NvbnRhaW5lci5qcyIsInNyYy9kcmFnYmFyLmpzIiwic3JjL2VsZW1lbnQuanMiLCJzcmMvZmxvYXQtd2luZG93LmpzIiwic3JjL2luZGV4LmpzIiwic3JjL3B1YnN1Yi5qcyIsInNyYy9yb3cuanMiLCJzcmMvdG9vbGJveC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgUHViU3ViID0gcmVxdWlyZSgnLi9wdWJzdWInKTtcbnZhciBUb29sYm94ID0gcmVxdWlyZSgnLi90b29sYm94Jyk7XG52YXIgQ29udGFpbmVyID0gcmVxdWlyZSgnLi9jb250YWluZXInKTtcblxuZnVuY3Rpb24gb25Nb3VzZURvd24oZSkge1xuICBQdWJTdWIucHVibGlzaCgnd2luZG93LW1vdXNlZG93bicsIGUpO1xufVxuXG5mdW5jdGlvbiBvbk1vdXNlVXAoZSkge1xuICBQdWJTdWIucHVibGlzaCgnd2luZG93LW1vdXNldXAnLCBlKTtcbn1cblxuZnVuY3Rpb24gb25Nb3VzZU1vdmUoZSkge1xuICBQdWJTdWIucHVibGlzaCgnd2luZG93LW1vdXNlbW92ZScsIGUpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgY29udHJvbGxlcjogZnVuY3Rpb24oYXJncykge1xuICAgIHZhciB0b29sYm94ID0gYXJncy50b29sYm94LFxuICAgICAgICBncmlkID0gYXJncy5ncmlkO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHRvb2xib3g6IG0ucHJvcCh0b29sYm94KSxcbiAgICAgIGdyaWQ6IG0ucHJvcChncmlkKSxcblxuICAgICAgcmVnaXN0ZXJHbG9iYWxFdmVudHM6IGZ1bmN0aW9uKGVsZW0sIGlzSW5pdCwgY3R4LCB2ZG9tKSB7XG4gICAgICAgIGlmICghaXNJbml0KSB7XG4gICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIG9uTW91c2VEb3duLCBmYWxzZSk7XG4gICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBvbk1vdXNlVXAsIGZhbHNlKTtcbiAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgb25Nb3VzZU1vdmUsIGZhbHNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGN0eC5vbnVubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCBvbk1vdXNlRG93biwgZmFsc2UpO1xuICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZXVwJywgb25Nb3VzZVVwLCBmYWxzZSk7XG4gICAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIG9uTW91c2VNb3ZlLCBmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuICB9LFxuXG4gIHZpZXc6IGZ1bmN0aW9uKGN0cmwsIGFyZ3MpIHtcbiAgICByZXR1cm4gbSgnLmFwcCcsIHtcbiAgICAgIGNvbmZpZzogY3RybC5yZWdpc3Rlckdsb2JhbEV2ZW50c1xuICAgIH0sIFtcbiAgICAgIC8vIG0oVG9vbGJveCwgY3RybC50b29sYm94KCkpLFxuICAgICAgbShDb250YWluZXIsIGN0cmwuZ3JpZCgpKVxuICAgIF0pO1xuICB9XG59O1xuIiwidmFyIENvbmZpZ3VyYWJsZUJveCA9IHJlcXVpcmUoJy4vY29uZmlndXJhYmxlLWJveCcpO1xudmFyIEVsZW1lbnQgPSByZXF1aXJlKCcuL2VsZW1lbnQnKTtcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIG1pbldpZHRoOiBtLnByb3AoMTAwIC8gMTIpLFxuXG4gIHZhbGlkYXRlOiBmdW5jdGlvbihjb2x1bW4pIHtcbiAgICByZXR1cm4gY29sdW1uIHx8IHt9O1xuICB9LFxuXG4gIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKGFyZ3MpIHtcbiAgICB2YXIgY29sdW1uID0gYXJncy5jb2x1bW4sXG4gICAgICAgIGN0cmw7XG5cbiAgICBjdHJsID0ge1xuICAgICAgc2V0Q29tcG9uZW50OiBmdW5jdGlvbihjb21wb25lbnQpIHtcbiAgICAgICAgY29tcG9uZW50ID0gRWxlbWVudC52YWxpZGF0ZShjb21wb25lbnQpO1xuICAgICAgICBjb2x1bW4uZWxlbWVudCA9IGNvbXBvbmVudDtcblxuICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gY3RybDtcbiAgfSxcblxuICB2aWV3OiBmdW5jdGlvbihjdHJsLCBhcmdzKSB7XG4gICAgdmFyIGNvbHVtbiA9IGFyZ3MuY29sdW1uLFxuICAgICAgICBlbGVtZW50ID0gYXJncy5jb2x1bW4uZWxlbWVudCB8fCB7fSxcbiAgICAgICAgY29sdW1uT3B0aW9ucyA9IGNvbHVtbiAmJiBjb2x1bW4ub3B0aW9ucyA/IGNvbHVtbi5vcHRpb25zIDoge30sXG4gICAgICAgIHBhZGRpbmcgPSBjb2x1bW5PcHRpb25zLnBhZGRpbmcgfHwgMDtcblxuICAgIHJldHVybiBtKCcuY29sdW1uLXdyYXBwZXInLCB7XG4gICAgICBrZXk6IGNvbHVtbi5pZCxcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIHdpZHRoOiBhcmdzLndpZHRoICsgJyUnXG4gICAgICB9XG4gICAgfSwgW1xuICAgICAgbShDb25maWd1cmFibGVCb3gsIHtcbiAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICBjbGFzc05hbWU6ICdjb2x1bW4nXG4gICAgICAgIH0sXG4gICAgICAgIGNvbnRlbnRzOiBlbGVtZW50LnR5cGUgPyBtKEVsZW1lbnQsIHsgY29sdW1uOiBhcmdzLmNvbHVtbiB9KSA6IG51bGxcbiAgICAgIH0pXG4gICAgXSk7XG4gIH1cbn07XG4iLCJ2YXIgUHViU3ViID0gcmVxdWlyZSgnLi9wdWJzdWInKTtcblxudmFyIGxhc3RIaWdobGlnaHRlZEN0cmw7XG5cbnZhciBDb25maWd1cmFibGVCb3ggPSBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgY29udHJvbGxlcjogZnVuY3Rpb24oYXJncykge1xuICAgIHZhciBjdHJsID0ge1xuICAgICAgaXNIaWdobGlnaHRlZDogbS5wcm9wKGZhbHNlKSxcblxuICAgICAgaGlnaGxpZ2h0OiBmdW5jdGlvbihlKSB7XG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgICAgaWYgKGxhc3RIaWdobGlnaHRlZEN0cmwpIHtcbiAgICAgICAgICBsYXN0SGlnaGxpZ2h0ZWRDdHJsLmlzSGlnaGxpZ2h0ZWQoZmFsc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgY3RybC5pc0hpZ2hsaWdodGVkKHRydWUpO1xuICAgICAgICBsYXN0SGlnaGxpZ2h0ZWRDdHJsID0gY3RybDtcblxuICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgfSxcblxuICAgICAgb25PcHRpb25zQ2xpY2s6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBhcmdzLmF0dHJzLm9ub3B0aW9uc2NsaWNrID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgYXJncy5hdHRycy5vbm9wdGlvbnNjbGljayhlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBQdWJTdWIuc3Vic2NyaWJlKCd3aW5kb3ctbW91c2Vkb3duJywgZnVuY3Rpb24oZSkge1xuICAgICAgY3RybC5pc0hpZ2hsaWdodGVkKGZhbHNlKTtcbiAgICAgIG0ucmVkcmF3KCk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gY3RybDtcbiAgfSxcblxuICB2aWV3OiBmdW5jdGlvbihjdHJsLCBhcmdzKSB7XG4gICAgdmFyIGNsYXNzTmFtZSA9ICcuY29uZmlndXJhYmxlLWJveCcgKyAoY3RybC5pc0hpZ2hsaWdodGVkKCkgPyAnIGhpZ2hsaWdodGVkJyA6ICcnKTtcblxuICAgIHJldHVybiBtKGNsYXNzTmFtZSwgT2JqZWN0LmFzc2lnbih7XG4gICAgICBvbm1vdXNlZG93bjogY3RybC5oaWdobGlnaHRcbiAgICB9LCBhcmdzLmF0dHJzKSwgW1xuICAgICAgYXJncy5jb250ZW50cyxcbiAgICAgIGN0cmwuaXNIaWdobGlnaHRlZCgpID8gbSgnLm9wdGlvbnMtdGFiJywgW1xuICAgICAgICBtKCdhLm9wdGlvbnMtbGluaycsIHtcbiAgICAgICAgICBocmVmOiAnIycsXG4gICAgICAgICAgb25jbGljazogY3RybC5vbk9wdGlvbnNDbGlja1xuICAgICAgICB9LCAnb3B0aW9ucycpXG4gICAgICBdKSA6IG51bGxcbiAgICBdKTtcbiAgfVxufTtcbiIsInZhciBDb25maWd1cmFibGVCb3ggPSByZXF1aXJlKCcuL2NvbmZpZ3VyYWJsZS1ib3gnKTtcbnZhciBSb3cgPSByZXF1aXJlKCcuL3JvdycpO1xuXG52YXIgQ29udGFpbmVyID0gbW9kdWxlLmV4cG9ydHMgPSB7XG4gIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKGFyZ3MpIHtcbiAgICB2YXIgY29udGFpbmVyID0gYXJncyxcbiAgICAgICAgY3RybDtcblxuICAgIGN0cmwgPSB7XG4gICAgICBjb250YWluZXI6IG0ucHJvcChjb250YWluZXIpLFxuICAgICAgY3VycmVudFJvd0lkOiBtLnByb3AoMCksXG5cbiAgICAgIGdldE5ld1Jvd0lkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIGN0cmwuY3VycmVudFJvd0lkKGN0cmwuY3VycmVudFJvd0lkKCkgKyAxKTtcbiAgICAgIH0sXG5cbiAgICAgIGFkZFJvdzogZnVuY3Rpb24ocm93KSB7XG4gICAgICAgIHJvdyA9IHJvdyB8fCB7fTtcbiAgICAgICAgcm93LmlkID0gY3RybC5nZXROZXdSb3dJZCgpO1xuICAgICAgICBjdHJsLmNvbnRhaW5lcigpLnJvd3MucHVzaChyb3cpO1xuICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgfSxcblxuICAgICAgcmVtb3ZlUm93OiBmdW5jdGlvbihpZCkge1xuICAgICAgICBjdHJsLmNvbnRhaW5lcigpLnJvd3MgPSBjdHJsLmNvbnRhaW5lcigpLnJvd3MuZmlsdGVyKGZ1bmN0aW9uKHJvdykge1xuICAgICAgICAgIHJldHVybiByb3cuaWQgIT0gaWQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIG0ucmVkcmF3KCk7XG4gICAgICB9LFxuXG4gICAgICBzaG93T3B0aW9uc01lbnU6IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2NvbnRhaW5lciBjbGlja2VkLicpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBjdHJsLmNvbnRhaW5lcigpLnJvd3MuZm9yRWFjaChmdW5jdGlvbihyb3cpIHtcbiAgICAgIHJvdy5pZCA9IGN0cmwuZ2V0TmV3Um93SWQoKTtcbiAgICB9KTtcblxuICAgIHJldHVybiBjdHJsO1xuICB9LFxuXG4gIHZpZXc6IGZ1bmN0aW9uKGN0cmwsIGFyZ3MpIHtcbiAgICByZXR1cm4gbShDb25maWd1cmFibGVCb3gsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIGNsYXNzTmFtZTogJ2NvbnRhaW5lcicsXG4gICAgICAgIG9ub3B0aW9uc2NsaWNrOiBjdHJsLnNob3dPcHRpb25zTWVudVxuICAgICAgfSxcbiAgICAgIGNvbnRlbnRzOiBjdHJsLmNvbnRhaW5lcigpLnJvd3MubWFwKGZ1bmN0aW9uKHJvdykge1xuICAgICAgICByZXR1cm4gbShSb3csIHsgcm93OiByb3cgfSk7XG4gICAgICB9KVxuICAgIH0pO1xuICB9XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSB7XG4gIHZpZXc6IGZ1bmN0aW9uKGN0cmwsIGFyZ3MpIHtcbiAgICB2YXIgd2lkdGggPSA0O1xuXG4gICAgcmV0dXJuIG0oJy5kcmFnYmFyJywge1xuICAgICAgc3R5bGU6IHtcbiAgICAgICAgdG9wOiAwLFxuICAgICAgICBib3R0b206IDAsXG4gICAgICAgIGxlZnQ6ICdjYWxjKCcgKyBhcmdzLnBvcyArICclIC0gJyArICh3aWR0aCAvIDIpICsgJ3B4KScsXG4gICAgICAgIHdpZHRoOiB3aWR0aCArICdweCdcbiAgICAgIH0sXG4gICAgICBvbm1vdXNlZG93bjogZnVuY3Rpb24oZSkge1xuICAgICAgICBhcmdzLm9uRHJhZ1N0YXJ0KGUsIGFyZ3Mucm93LCBhcmdzLmNvbEluZGV4KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxufTtcbiIsInZhciBDb25maWd1cmFibGVCb3ggPSByZXF1aXJlKCcuL2NvbmZpZ3VyYWJsZS1ib3gnKTtcblxudmFyIEVsZW1lbnQgPSBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgdmFsaWRhdGU6IGZ1bmN0aW9uKGVsZW1lbnQpIHtcbiAgICByZXR1cm4gZWxlbWVudCB8fCB7fTtcbiAgfSxcblxuICBjb250cm9sbGVyOiBmdW5jdGlvbihhcmdzKSB7XG4gICAgdmFyIGN0cmwgPSB7XG4gICAgICBjb2x1bW46IG0ucHJvcChhcmdzLmNvbHVtbiksXG4gICAgICBlbGVtZW50OiBtLnByb3AoRWxlbWVudC52YWxpZGF0ZShhcmdzLmNvbHVtbi5lbGVtZW50KSlcbiAgICB9O1xuXG4gICAgcmV0dXJuIGN0cmw7XG4gIH0sXG5cbiAgdmlldzogZnVuY3Rpb24oY3RybCwgYXJncykge1xuICAgIHZhciBjbGFzc05hbWUgPSAnZWxlbWVudCc7XG5cbiAgICByZXR1cm4gbShDb25maWd1cmFibGVCb3gsIHtcbiAgICAgIGF0dHJzOiB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lXG4gICAgICB9LFxuICAgICAgY29udGVudHM6IGN0cmwuZWxlbWVudCgpLnR5cGUgPyBjdHJsLmNvbHVtbigpIDogbnVsbFxuICAgIH0pO1xuICB9XG59O1xuIiwidmFyIFZNID0gZnVuY3Rpb24ocG9zKSB7XG4gIHZhciB2bSA9IHtcbiAgICB4OiBtLnByb3AocG9zLngpLFxuICAgIHk6IG0ucHJvcChwb3MueSksXG4gICAgekluZGV4OiBtLnByb3AoVk0ubGFzdE1heFpJbmRleCksXG4gICAgZHJhZ1N0YXJ0SW5mbzogbS5wcm9wKG51bGwpXG4gIH07XG5cbiAgcmV0dXJuIHZtO1xufTtcblxuVk0ubGFzdE1heFpJbmRleCA9IDEwMDAwO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgY29udHJvbGxlcjogZnVuY3Rpb24oYXJncykge1xuICAgIHZhciB2bSA9IFZNKGFyZ3MucG9zKTtcblxuICAgIHZhciBjdHJsID0ge1xuICAgICAgeDogdm0ueCxcbiAgICAgIHk6IHZtLnksXG4gICAgICB6SW5kZXg6IHZtLnpJbmRleCxcbiAgICAgIGRyYWdTdGFydEluZm86IHZtLmRyYWdTdGFydEluZm8sXG5cbiAgICAgIG9uRHJhZ1N0YXJ0OiBmdW5jdGlvbihlKSB7XG4gICAgICAgIGZ1bmN0aW9uIG9uRHJhZ2dpbmcoZSkge1xuICAgICAgICAgIGN0cmwueChjdHJsLmRyYWdTdGFydEluZm8oKS54ICsgZS5jbGllbnRYIC0gY3RybC5kcmFnU3RhcnRJbmZvKCkuY2xpZW50WCk7XG4gICAgICAgICAgY3RybC55KGN0cmwuZHJhZ1N0YXJ0SW5mbygpLnkgKyBlLmNsaWVudFkgLSBjdHJsLmRyYWdTdGFydEluZm8oKS5jbGllbnRZKTtcblxuICAgICAgICAgIG0ucmVkcmF3KCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBvbkRyYWdFbmQoZSkge1xuICAgICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIG9uRHJhZ2dpbmcsIHRydWUpO1xuICAgICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBvbkRyYWdFbmQsIHRydWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGN0cmwuZHJhZ1N0YXJ0SW5mbyh7XG4gICAgICAgICAgeDogY3RybC54KCksXG4gICAgICAgICAgeTogY3RybC55KCksXG4gICAgICAgICAgY2xpZW50WDogZS5jbGllbnRYLFxuICAgICAgICAgIGNsaWVudFk6IGUuY2xpZW50WVxuICAgICAgICB9KTtcblxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBvbkRyYWdnaW5nLCB0cnVlKTtcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2V1cCcsIG9uRHJhZ0VuZCwgdHJ1ZSk7XG5cbiAgICAgICAgY3RybC5sYXN0RG9jdW1lbnRDdXJzb3IgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuY3Vyc29yO1xuICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuY3Vyc29yID0gJ21vdmUnO1xuXG4gICAgICAgIGN0cmwuekluZGV4KFZNLmxhc3RNYXhaSW5kZXgrKyk7XG5cbiAgICAgICAgbS5yZWRyYXcoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIGN0cmw7XG4gIH0sXG5cbiAgdmlldzogZnVuY3Rpb24oY3RybCwgYXJncykge1xuICAgIHJldHVybiBtKCcuZmxvYXQtd2luZG93Jywge1xuICAgICAgc3R5bGU6IHtcbiAgICAgICAgbGVmdDogY3RybC54KCkgKyAncHgnLFxuICAgICAgICB0b3A6IGN0cmwueSgpICsgJ3B4JyxcbiAgICAgICAgekluZGV4OiBjdHJsLnpJbmRleCgpXG4gICAgICB9XG4gICAgfSwgW1xuICAgICAgbSgnLmZsb2F0LXdpbmRvdy10aXRsZWJhcicsIHtcbiAgICAgICAgb25tb3VzZWRvd246IGN0cmwub25EcmFnU3RhcnRcbiAgICAgIH0sIGFyZ3MudGl0bGUpLFxuICAgICAgbSgnLmZsb2F0LXdpbmRvdy1jb250ZW50JywgYXJncy5jb250ZW50KVxuICAgIF0pO1xuICB9XG59O1xuIiwidmFyIEFwcCA9IHJlcXVpcmUoJy4vYXBwJyk7XG5cbnZhciBCdWlsZGVyID0ge1xuICBjb250cm9sbGVyOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgZGF0YSA9IGJzRGF0YTtcblxuICAgIHJldHVybiB7XG4gICAgICBkYXRhOiBtLnByb3AoZGF0YSlcbiAgICB9O1xuICB9LFxuXG4gIHZpZXc6IGZ1bmN0aW9uKGN0cmwpIHtcbiAgICByZXR1cm4gbShBcHAsIGN0cmwuZGF0YSgpKTtcbiAgfVxufTtcblxubS5tb3VudChkb2N1bWVudC5ib2R5LCBCdWlsZGVyKTtcbiIsInZhciB0b3BpY3MgPSB7fTtcbnZhciBoT1AgPSB0b3BpY3MuaGFzT3duUHJvcGVydHk7XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBzdWJzY3JpYmU6IGZ1bmN0aW9uKHRvcGljLCBsaXN0ZW5lcikge1xuICAgIC8vIENyZWF0ZSB0aGUgdG9waWMncyBvYmplY3QgaWYgbm90IHlldCBjcmVhdGVkXG4gICAgaWYoIWhPUC5jYWxsKHRvcGljcywgdG9waWMpKSB0b3BpY3NbdG9waWNdID0gW107XG5cbiAgICAvLyBBZGQgdGhlIGxpc3RlbmVyIHRvIHF1ZXVlXG4gICAgdmFyIGluZGV4ID0gdG9waWNzW3RvcGljXS5wdXNoKGxpc3RlbmVyKSAtMTtcblxuICAgIC8vIFByb3ZpZGUgaGFuZGxlIGJhY2sgZm9yIHJlbW92YWwgb2YgdG9waWNcbiAgICByZXR1cm4ge1xuICAgICAgcmVtb3ZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgZGVsZXRlIHRvcGljc1t0b3BpY11baW5kZXhdO1xuICAgICAgfVxuICAgIH07XG4gIH0sXG5cbiAgcHVibGlzaDogZnVuY3Rpb24odG9waWMsIGluZm8pIHtcbiAgICAvLyBJZiB0aGUgdG9waWMgZG9lc24ndCBleGlzdCwgb3IgdGhlcmUncyBubyBsaXN0ZW5lcnMgaW4gcXVldWUsIGp1c3QgbGVhdmVcbiAgICBpZighaE9QLmNhbGwodG9waWNzLCB0b3BpYykpIHJldHVybjtcblxuICAgIC8vIEN5Y2xlIHRocm91Z2ggdG9waWNzIHF1ZXVlLCBmaXJlIVxuICAgIHRvcGljc1t0b3BpY10uZm9yRWFjaChmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgIGl0ZW0oaW5mbyAhPSB1bmRlZmluZWQgPyBpbmZvIDoge30pO1xuICAgIH0pO1xuICB9XG59O1xuIiwidmFyIFB1YlN1YiA9IHJlcXVpcmUoJy4vcHVic3ViJyk7XG52YXIgQ29uZmlndXJhYmxlQm94ID0gcmVxdWlyZSgnLi9jb25maWd1cmFibGUtYm94Jyk7XG52YXIgQ29sdW1uID0gcmVxdWlyZSgnLi9jb2x1bW4nKTtcbnZhciBEcmFnYmFyID0gcmVxdWlyZSgnLi9kcmFnYmFyJyk7XG5cbnZhciBSb3cgPSBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgdmFsaWRhdGU6IGZ1bmN0aW9uKHJvdykge1xuICAgIHZhciBvcHRpb25zID0gcm93Lm9wdGlvbnMgfHwge30sXG4gICAgICAgIGNvbHVtbnNObyA9IG9wdGlvbnMuY29sdW1uc05vIHx8IDEsXG4gICAgICAgIGNvbHVtbnMgPSByb3cuY29sdW1ucyB8fCBbXTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY29sdW1uc05vOyBpKyspIHtcbiAgICAgIGNvbHVtbnNbaV0gPSBDb2x1bW4udmFsaWRhdGUoY29sdW1uc1tpXSk7XG4gICAgfVxuXG4gICAgcm93LmNvbHVtbnMgPSBjb2x1bW5zO1xuICB9LFxuXG4gIGNvbnRyb2xsZXI6IGZ1bmN0aW9uKGFyZ3MpIHtcbiAgICB2YXIgcm93ID0gYXJncy5yb3csXG4gICAgICAgIGN0cmw7XG5cbiAgICBjdHJsID0ge1xuICAgICAgbGFzdERyYWdTdGFydEluZm86IG0ucHJvcChudWxsKSxcbiAgICAgIGN1cnJlbnRDb2xJZDogbS5wcm9wKDApLFxuXG4gICAgICBnZXROZXdDb2xJZDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBjdHJsLmN1cnJlbnRDb2xJZChjdHJsLmN1cnJlbnRDb2xJZCgpICsgMSk7XG4gICAgICB9LFxuXG4gICAgICBzZXRDb2x1bW5zOiBmdW5jdGlvbihjb2x1bW5zKSB7XG4gICAgICAgIGlmICghY29sdW1ucyB8fCBjb2x1bW5zLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICBjb2x1bW5zID0gW3t9XTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJvdy5jb2x1bW5zID0gY29sdW1ucztcbiAgICAgICAgcm93Lm9wdGlvbnMuY29sdW1uc05vID0gY29sdW1ucy5sZW5ndGg7XG4gICAgICAgIG0ucmVkcmF3KCk7XG4gICAgICB9LFxuXG4gICAgICB2YWxpZGF0ZUNvbHVtbnNXaWR0aDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBvcHRpb25zID0gcm93Lm9wdGlvbnMgfHwge30sXG4gICAgICAgICAgICBjb2x1bW5zTm8gPSBvcHRpb25zLmNvbHVtbnNObyB8fCAxLFxuICAgICAgICAgICAgc3VtT2ZDb2x1bW5zV2l0aFdpZHRoID0gMCxcbiAgICAgICAgICAgIGNvbHVtbnNXaXRoV2lkdGhObyA9IDA7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb2x1bW5zTm87IGkrKykge1xuICAgICAgICAgIHZhciBjb2x1bW4gPSByb3cuY29sdW1uc1tpXSB8fCB7fSxcbiAgICAgICAgICAgICAgd2lkdGggPSBwYXJzZUZsb2F0KGNvbHVtbi53aWR0aCk7XG5cbiAgICAgICAgICBpZiAod2lkdGgpIHtcbiAgICAgICAgICAgIHN1bU9mQ29sdW1uc1dpdGhXaWR0aCArPSB3aWR0aDtcbiAgICAgICAgICAgIGNvbHVtbnNXaXRoV2lkdGhObysrO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHJvdy5jb2x1bW5zW2ldID0gY29sdW1uO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHdpZHRoT2ZDb2x1bW5zV2l0aE5vV2lkdGggPSAoMTAwIC0gc3VtT2ZDb2x1bW5zV2l0aFdpZHRoKSAvIChjb2x1bW5zTm8gLSBjb2x1bW5zV2l0aFdpZHRoTm8pO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBsZW5ndGggPSByb3cuY29sdW1ucy5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICAgIHJvdy5jb2x1bW5zW2ldLndpZHRoID0gcm93LmNvbHVtbnNbaV0ud2lkdGggfHwgd2lkdGhPZkNvbHVtbnNXaXRoTm9XaWR0aDtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgb25EcmFnU3RhcnQ6IGZ1bmN0aW9uKGUsIHJvdywgY29sSW5kZXgpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuXG4gICAgICAgIGZ1bmN0aW9uIG9uRHJhZ2dpbmcoZSkge1xuICAgICAgICAgIHZhciBkcmFnU3RhcnRJbmZvID0gY3RybC5sYXN0RHJhZ1N0YXJ0SW5mbygpLFxuICAgICAgICAgICAgICBjb2xJbmRleCA9IGRyYWdTdGFydEluZm8uY29sSW5kZXgsXG4gICAgICAgICAgICAgIHJvd1dpZHRoID0gZHJhZ1N0YXJ0SW5mby5yb3dXaWR0aCxcbiAgICAgICAgICAgICAgbW92ZVggPSBlLmNsaWVudFggLSBkcmFnU3RhcnRJbmZvLmNsaWVudFgsXG4gICAgICAgICAgICAgIGNvbE1pbldpZHRoID0gQ29sdW1uLm1pbldpZHRoKCksXG4gICAgICAgICAgICAgIHN1bU9mVHdvQ29scyA9IGRyYWdTdGFydEluZm8uY3VycmVudENvbFdpZHRoICsgZHJhZ1N0YXJ0SW5mby5uZXh0Q29sV2lkdGgsXG4gICAgICAgICAgICAgIHBlcmNlbnQgPSBtb3ZlWCAvIHJvd1dpZHRoICogMTAwO1xuXG4gICAgICAgICAgaWYgKGRyYWdTdGFydEluZm8uY3VycmVudENvbFdpZHRoICsgcGVyY2VudCA8IGNvbE1pbldpZHRoKSB7XG4gICAgICAgICAgICByb3cuY29sdW1uc1tjb2xJbmRleF0ud2lkdGggPSBjb2xNaW5XaWR0aDtcbiAgICAgICAgICAgIHJvdy5jb2x1bW5zW2NvbEluZGV4ICsgMV0ud2lkdGggPSBzdW1PZlR3b0NvbHMgLSBjb2xNaW5XaWR0aDtcbiAgICAgICAgICB9IGVsc2UgaWYgKGRyYWdTdGFydEluZm8ubmV4dENvbFdpZHRoIC0gcGVyY2VudCA8IGNvbE1pbldpZHRoKSB7XG4gICAgICAgICAgICByb3cuY29sdW1uc1tjb2xJbmRleF0ud2lkdGggPSBzdW1PZlR3b0NvbHMgLSBjb2xNaW5XaWR0aDtcbiAgICAgICAgICAgIHJvdy5jb2x1bW5zW2NvbEluZGV4ICsgMV0ud2lkdGggPSBjb2xNaW5XaWR0aDtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcm93LmNvbHVtbnNbY29sSW5kZXhdLndpZHRoID0gZHJhZ1N0YXJ0SW5mby5jdXJyZW50Q29sV2lkdGggKyBwZXJjZW50O1xuICAgICAgICAgICAgcm93LmNvbHVtbnNbY29sSW5kZXggKyAxXS53aWR0aCA9IGRyYWdTdGFydEluZm8ubmV4dENvbFdpZHRoIC0gcGVyY2VudDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBtLnJlZHJhdygpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gb25EcmFnRW5kKGUpIHtcbiAgICAgICAgICBtb3VzZU1vdmVIYW5kbGVyLnJlbW92ZSgpO1xuICAgICAgICAgIG1vdXNlVXBIYW5kbGVyLnJlbW92ZSgpO1xuXG4gICAgICAgICAgY3RybC5sYXN0RHJhZ1N0YXJ0SW5mbyhudWxsKTtcblxuICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZS5jdXJzb3IgPSBjdHJsLmxhc3REb2N1bWVudEN1cnNvcjtcbiAgICAgICAgfVxuXG4gICAgICAgIGN0cmwubGFzdERyYWdTdGFydEluZm8oe1xuICAgICAgICAgIHJvd1dpZHRoOiBlLnRhcmdldC5wYXJlbnROb2RlLm9mZnNldFdpZHRoLFxuICAgICAgICAgIGN1cnJlbnRDb2xXaWR0aDogcm93LmNvbHVtbnNbY29sSW5kZXhdLndpZHRoLFxuICAgICAgICAgIG5leHRDb2xXaWR0aDogcm93LmNvbHVtbnNbY29sSW5kZXggKyAxXS53aWR0aCxcbiAgICAgICAgICBjb2xJbmRleDogY29sSW5kZXgsXG4gICAgICAgICAgb2Zmc2V0WDogZS5vZmZzZXRYLFxuICAgICAgICAgIG9mZnNldFk6IGUub2Zmc2V0WSxcbiAgICAgICAgICBjbGllbnRYOiBlLmNsaWVudFgsXG4gICAgICAgICAgY2xpZW50WTogZS5jbGllbnRZXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHZhciBtb3VzZU1vdmVIYW5kbGVyID0gUHViU3ViLnN1YnNjcmliZSgnd2luZG93LW1vdXNlbW92ZScsIG9uRHJhZ2dpbmcpO1xuICAgICAgICB2YXIgbW91c2VVcEhhbmRsZXIgPSBQdWJTdWIuc3Vic2NyaWJlKCd3aW5kb3ctbW91c2V1cCcsIG9uRHJhZ0VuZCk7XG5cbiAgICAgICAgY3RybC5sYXN0RG9jdW1lbnRDdXJzb3IgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuY3Vyc29yO1xuICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuY3Vyc29yID0gJ2NvbC1yZXNpemUnO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBSb3cudmFsaWRhdGUocm93KTtcblxuICAgIHJvdy5jb2x1bW5zLmZvckVhY2goZnVuY3Rpb24oY29sdW1uKSB7XG4gICAgICBjb2x1bW4uaWQgPSBjdHJsLmdldE5ld0NvbElkKCk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gY3RybDtcbiAgfSxcblxuICB2aWV3OiBmdW5jdGlvbihjdHJsLCBhcmdzKSB7XG4gICAgdmFyIHJvdyA9IGFyZ3Mucm93LFxuICAgICAgICBlbGVtcyA9IFtdLFxuICAgICAgICBvcHRpb25zID0gcm93Lm9wdGlvbnMgfHwge30sXG4gICAgICAgIGNvbHVtbnNObyA9IG9wdGlvbnMuY29sdW1uc05vIHx8IDEsXG4gICAgICAgIHdpZHRoU3VtID0gMDtcblxuICAgIGN0cmwudmFsaWRhdGVDb2x1bW5zV2lkdGgoKTtcblxuICAgIGZvciAodmFyIGNvbEluZGV4ID0gMDsgY29sSW5kZXggPCBjb2x1bW5zTm87IGNvbEluZGV4KyspIHtcbiAgICAgIHZhciBjb2x1bW4gPSByb3cuY29sdW1uc1tjb2xJbmRleF0sXG4gICAgICAgICAgY29sdW1uV2lkdGggPSBjb2x1bW4ud2lkdGg7XG5cbiAgICAgIHJvdy5jb2x1bW5zW2NvbEluZGV4XS53aWR0aCA9IGNvbHVtbldpZHRoO1xuICAgICAgd2lkdGhTdW0gKz0gY29sdW1uV2lkdGg7XG5cbiAgICAgIGVsZW1zLnB1c2gobShDb2x1bW4sIHtcbiAgICAgICAgY29sdW1uOiBjb2x1bW4sXG4gICAgICAgIHdpZHRoOiBjb2x1bW5XaWR0aFxuICAgICAgfSkpO1xuXG4gICAgICBpZiAoY29sSW5kZXggPCBjb2x1bW5zTm8gLSAxKSB7XG4gICAgICAgIGVsZW1zLnB1c2gobShEcmFnYmFyLCB7XG4gICAgICAgICAgcm93OiByb3csXG4gICAgICAgICAgcG9zOiB3aWR0aFN1bSxcbiAgICAgICAgICBjb2xJbmRleDogY29sSW5kZXgsXG4gICAgICAgICAgb25EcmFnU3RhcnQ6IGN0cmwub25EcmFnU3RhcnRcbiAgICAgICAgfSkpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBtKENvbmZpZ3VyYWJsZUJveCwge1xuICAgICAgYXR0cnM6IHtcbiAgICAgICAgY2xhc3NOYW1lOiAncm93LXdyYXBwZXInXG4gICAgICB9LFxuICAgICAgY29udGVudHM6IG0oJy5yb3cnLCBlbGVtcylcbiAgICB9KTtcbiAgfVxufTtcbiIsInZhciBGbG9hdFdpbmRvdyA9IHJlcXVpcmUoJy4vZmxvYXQtd2luZG93Jyk7XG52YXIgRWxlbWVudCA9IHJlcXVpcmUoJy4vZWxlbWVudCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgdmlldzogZnVuY3Rpb24oY3RybCwgYXJncykge1xuICAgIHJldHVybiBtKEZsb2F0V2luZG93LCB7XG4gICAgICB0aXRsZTogJ0VsZW1lbnRzIExpc3QnLFxuICAgICAgcG9zOiB7XG4gICAgICAgIHg6IDE3MCxcbiAgICAgICAgeTogMTUwXG4gICAgICB9LFxuICAgICAgY29udGVudDogYXJncy50b29scy5tYXAoZnVuY3Rpb24odG9vbCkge1xuICAgICAgICByZXR1cm4gbShFbGVtZW50LCB0b29sKTtcbiAgICAgIH0pXG4gICAgfSk7XG4gIH1cbn07XG4iXX0=
