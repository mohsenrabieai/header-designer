var FloatWindow = require('./float-window');
var Element = require('./element');

module.exports = {
  view: function(ctrl, args) {
    return m(FloatWindow, {
      title: 'Elements List',
      pos: {
        x: 170,
        y: 150
      },
      content: args.tools.map(function(tool) {
        return m(Element, tool);
      })
    });
  }
};
