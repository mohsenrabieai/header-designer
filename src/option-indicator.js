var OptionIndicator = {
  controller: function(args) {
    var ctrl = {
      elem: m.prop(null),

      show: function(elem) {
        ctrl.elem(elem);
      },

      hide: function() {
        ctrl.elem(null);
      }
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    var rect = ctrl.elem().getBoundingClientRect();

    return m('.option-indicator', {
      style: Object.assign({
        position: 'absolute',
        boxSizing: 'border-box',
        border: '1px solid blue',
        zIndex: 1000
      }, rect)
    });
  }
};

