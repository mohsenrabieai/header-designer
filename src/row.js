var PubSub = require('./pubsub');
var ConfigurableBox = require('./configurable-box');
var Column = require('./column');
var Dragbar = require('./dragbar');

var Row = module.exports = {
  validate: function(row) {
    var options = row.options || {},
        columnsNo = options.columnsNo || 1,
        columns = row.columns || [];

    for (var i = 0; i < columnsNo; i++) {
      columns[i] = Column.validate(columns[i]);
    }

    row.columns = columns;
  },

  controller: function(args) {
    var row = args.row,
        ctrl;

    ctrl = {
      lastDragStartInfo: m.prop(null),
      currentColId: m.prop(0),

      getNewColId: function() {
        return ctrl.currentColId(ctrl.currentColId() + 1);
      },

      setColumns: function(columns) {
        if (!columns || columns.length < 1) {
          columns = [{}];
        }

        row.columns = columns;
        row.options.columnsNo = columns.length;
        m.redraw();
      },

      validateColumnsWidth: function() {
        var options = row.options || {},
            columnsNo = options.columnsNo || 1,
            sumOfColumnsWithWidth = 0,
            columnsWithWidthNo = 0;

        for (var i = 0; i < columnsNo; i++) {
          var column = row.columns[i] || {},
              width = parseFloat(column.width);

          if (width) {
            sumOfColumnsWithWidth += width;
            columnsWithWidthNo++;
          }

          row.columns[i] = column;
        }

        var widthOfColumnsWithNoWidth = (100 - sumOfColumnsWithWidth) / (columnsNo - columnsWithWidthNo);

        for (var i = 0, length = row.columns.length; i < length; i++) {
          row.columns[i].width = row.columns[i].width || widthOfColumnsWithNoWidth;
        }
      },

      onDragStart: function(e, row, colIndex) {
        e.preventDefault();
        e.stopPropagation();

        function onDragging(e) {
          var dragStartInfo = ctrl.lastDragStartInfo(),
              colIndex = dragStartInfo.colIndex,
              rowWidth = dragStartInfo.rowWidth,
              moveX = e.clientX - dragStartInfo.clientX,
              colMinWidth = Column.minWidth(),
              sumOfTwoCols = dragStartInfo.currentColWidth + dragStartInfo.nextColWidth,
              percent = moveX / rowWidth * 100;

          if (dragStartInfo.currentColWidth + percent < colMinWidth) {
            row.columns[colIndex].width = colMinWidth;
            row.columns[colIndex + 1].width = sumOfTwoCols - colMinWidth;
          } else if (dragStartInfo.nextColWidth - percent < colMinWidth) {
            row.columns[colIndex].width = sumOfTwoCols - colMinWidth;
            row.columns[colIndex + 1].width = colMinWidth;
          } else {
            row.columns[colIndex].width = dragStartInfo.currentColWidth + percent;
            row.columns[colIndex + 1].width = dragStartInfo.nextColWidth - percent;
          }

          m.redraw();
        }

        function onDragEnd(e) {
          mouseMoveHandler.remove();
          mouseUpHandler.remove();

          ctrl.lastDragStartInfo(null);

          document.documentElement.style.cursor = ctrl.lastDocumentCursor;
        }

        ctrl.lastDragStartInfo({
          rowWidth: e.target.parentNode.offsetWidth,
          currentColWidth: row.columns[colIndex].width,
          nextColWidth: row.columns[colIndex + 1].width,
          colIndex: colIndex,
          offsetX: e.offsetX,
          offsetY: e.offsetY,
          clientX: e.clientX,
          clientY: e.clientY
        });

        var mouseMoveHandler = PubSub.subscribe('window-mousemove', onDragging);
        var mouseUpHandler = PubSub.subscribe('window-mouseup', onDragEnd);

        ctrl.lastDocumentCursor = document.documentElement.style.cursor;
        document.documentElement.style.cursor = 'col-resize';
      }
    };

    Row.validate(row);

    row.columns.forEach(function(column) {
      column.id = ctrl.getNewColId();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    var row = args.row,
        elems = [],
        options = row.options || {},
        columnsNo = options.columnsNo || 1,
        widthSum = 0;

    ctrl.validateColumnsWidth();

    for (var colIndex = 0; colIndex < columnsNo; colIndex++) {
      var column = row.columns[colIndex],
          columnWidth = column.width;

      row.columns[colIndex].width = columnWidth;
      widthSum += columnWidth;

      elems.push(m(Column, {
        column: column,
        width: columnWidth
      }));

      if (colIndex < columnsNo - 1) {
        elems.push(m(Dragbar, {
          row: row,
          pos: widthSum,
          colIndex: colIndex,
          onDragStart: ctrl.onDragStart
        }));
      }
    }

    return m(ConfigurableBox, {
      attrs: {
        className: 'row-wrapper'
      },
      contents: m('.row', elems)
    });
  }
};
