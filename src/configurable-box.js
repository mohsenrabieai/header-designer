var PubSub = require('./pubsub');

var lastHighlightedCtrl;

var ConfigurableBox = module.exports = {
  controller: function(args) {
    var ctrl = {
      isHighlighted: m.prop(false),

      highlight: function(e) {
        e.stopPropagation();

        if (lastHighlightedCtrl) {
          lastHighlightedCtrl.isHighlighted(false);
        }

        ctrl.isHighlighted(true);
        lastHighlightedCtrl = ctrl;

        m.redraw();
      },

      onOptionsClick: function(e) {
        if (typeof args.attrs.onoptionsclick === 'function') {
          args.attrs.onoptionsclick(e);
        }
      }
    };

    PubSub.subscribe('window-mousedown', function(e) {
      ctrl.isHighlighted(false);
      m.redraw();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    var className = '.configurable-box' + (ctrl.isHighlighted() ? ' highlighted' : '');

    return m(className, Object.assign({
      onmousedown: ctrl.highlight
    }, args.attrs), [
      args.contents,
      ctrl.isHighlighted() ? m('.options-tab', [
        m('a.options-link', {
          href: '#',
          onclick: ctrl.onOptionsClick
        }, 'options')
      ]) : null
    ]);
  }
};
