var VM = function(pos) {
  var vm = {
    x: m.prop(pos.x),
    y: m.prop(pos.y),
    zIndex: m.prop(VM.lastMaxZIndex),
    dragStartInfo: m.prop(null)
  };

  return vm;
};

VM.lastMaxZIndex = 10000;

module.exports = {
  controller: function(args) {
    var vm = VM(args.pos);

    var ctrl = {
      x: vm.x,
      y: vm.y,
      zIndex: vm.zIndex,
      dragStartInfo: vm.dragStartInfo,

      onDragStart: function(e) {
        function onDragging(e) {
          ctrl.x(ctrl.dragStartInfo().x + e.clientX - ctrl.dragStartInfo().clientX);
          ctrl.y(ctrl.dragStartInfo().y + e.clientY - ctrl.dragStartInfo().clientY);

          m.redraw();
        }

        function onDragEnd(e) {
          document.removeEventListener('mousemove', onDragging, true);
          document.removeEventListener('mouseup', onDragEnd, true);
        }

        e.preventDefault();

        ctrl.dragStartInfo({
          x: ctrl.x(),
          y: ctrl.y(),
          clientX: e.clientX,
          clientY: e.clientY
        });

        document.addEventListener('mousemove', onDragging, true);
        document.addEventListener('mouseup', onDragEnd, true);

        ctrl.lastDocumentCursor = document.documentElement.style.cursor;
        document.documentElement.style.cursor = 'move';

        ctrl.zIndex(VM.lastMaxZIndex++);

        m.redraw();
      }
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    return m('.float-window', {
      style: {
        left: ctrl.x() + 'px',
        top: ctrl.y() + 'px',
        zIndex: ctrl.zIndex()
      }
    }, [
      m('.float-window-titlebar', {
        onmousedown: ctrl.onDragStart
      }, args.title),
      m('.float-window-content', args.content)
    ]);
  }
};
