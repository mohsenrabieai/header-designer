var ConfigurableBox = require('./configurable-box');
var Element = require('./element');

module.exports = {
  minWidth: m.prop(100 / 12),

  validate: function(column) {
    return column || {};
  },

  controller: function(args) {
    var column = args.column,
        ctrl;

    ctrl = {
      setComponent: function(component) {
        component = Element.validate(component);
        column.element = component;

        m.redraw();
      }
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    var column = args.column,
        element = args.column.element || {},
        columnOptions = column && column.options ? column.options : {},
        padding = columnOptions.padding || 0;

    return m('.column-wrapper', {
      key: column.id,
      style: {
        width: args.width + '%'
      }
    }, [
      m(ConfigurableBox, {
        attrs: {
          className: 'column'
        },
        contents: element.type ? m(Element, { column: args.column }) : null
      })
    ]);
  }
};
