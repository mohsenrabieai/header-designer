module.exports = {
  view: function(ctrl, args) {
    var width = 4;

    return m('.dragbar', {
      style: {
        top: 0,
        bottom: 0,
        left: 'calc(' + args.pos + '% - ' + (width / 2) + 'px)',
        width: width + 'px'
      },
      onmousedown: function(e) {
        args.onDragStart(e, args.row, args.colIndex);
      }
    });
  }
};
