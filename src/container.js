var ConfigurableBox = require('./configurable-box');
var Row = require('./row');

var Container = module.exports = {
  controller: function(args) {
    var container = args,
        ctrl;

    ctrl = {
      container: m.prop(container),
      currentRowId: m.prop(0),

      getNewRowId: function() {
        return ctrl.currentRowId(ctrl.currentRowId() + 1);
      },

      addRow: function(row) {
        row = row || {};
        row.id = ctrl.getNewRowId();
        ctrl.container().rows.push(row);
        m.redraw();
      },

      removeRow: function(id) {
        ctrl.container().rows = ctrl.container().rows.filter(function(row) {
          return row.id != id;
        });

        m.redraw();
      },

      showOptionsMenu: function(e) {
        console.log('container clicked.');
      }
    };

    ctrl.container().rows.forEach(function(row) {
      row.id = ctrl.getNewRowId();
    });

    return ctrl;
  },

  view: function(ctrl, args) {
    return m(ConfigurableBox, {
      attrs: {
        className: 'container',
        onoptionsclick: ctrl.showOptionsMenu
      },
      contents: ctrl.container().rows.map(function(row) {
        return m(Row, { row: row });
      })
    });
  }
};
