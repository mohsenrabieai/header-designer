var PubSub = require('./pubsub');
var Toolbox = require('./toolbox');
var Container = require('./container');

function onMouseDown(e) {
  PubSub.publish('window-mousedown', e);
}

function onMouseUp(e) {
  PubSub.publish('window-mouseup', e);
}

function onMouseMove(e) {
  PubSub.publish('window-mousemove', e);
}

module.exports = {
  controller: function(args) {
    var toolbox = args.toolbox,
        grid = args.grid;

    return {
      toolbox: m.prop(toolbox),
      grid: m.prop(grid),

      registerGlobalEvents: function(elem, isInit, ctx, vdom) {
        if (!isInit) {
          window.addEventListener('mousedown', onMouseDown, false);
          window.addEventListener('mouseup', onMouseUp, false);
          window.addEventListener('mousemove', onMouseMove, false);
        }

        ctx.onunload = function() {
          window.removeEventListener('mousedown', onMouseDown, false);
          window.removeEventListener('mouseup', onMouseUp, false);
          window.removeEventListener('mousemove', onMouseMove, false);
        }
      }
    };
  },

  view: function(ctrl, args) {
    return m('.app', {
      config: ctrl.registerGlobalEvents
    }, [
      // m(Toolbox, ctrl.toolbox()),
      m(Container, ctrl.grid())
    ]);
  }
};
