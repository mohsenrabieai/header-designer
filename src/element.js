var ConfigurableBox = require('./configurable-box');

var Element = module.exports = {
  validate: function(element) {
    return element || {};
  },

  controller: function(args) {
    var ctrl = {
      column: m.prop(args.column),
      element: m.prop(Element.validate(args.column.element))
    };

    return ctrl;
  },

  view: function(ctrl, args) {
    var className = 'element';

    return m(ConfigurableBox, {
      attrs: {
        className: className
      },
      contents: ctrl.element().type ? ctrl.column() : null
    });
  }
};
