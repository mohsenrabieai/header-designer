# Header Builder ArtBees
Mithril.js based header builder app for Artbees Themes

### Instruction to run on Windows

1- Remove node_modules folder 

2- Run "npm install" under Administrator level

3- Run "npm install -g browsify" 

4- Run "npm install -g watchify" 

5- Change "package.json" line 8 :    
```
"start": "node server.js & watchify --debug src/index.js -o dist/bundle.js" 
```
to
```
"start": "start.bat"
```
6- Create start.bat and put these lines in it:
```
start node server.js
start watchify --debug src/index.js -o dist/bundle.js
```
7- Run "npm start"

If port 8080 was already in use, please change the port at server.js line 11
```
var portNo = 8080;
```
After these steps you should watchify will track your changes and do a compile the whole source code to "bundle.js" so you don't need to restart node every time you change something.
